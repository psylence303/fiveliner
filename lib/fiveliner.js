import fs from 'fs';
import split from 'split';

module.exports = function() {

  /**
   * Reading the file using stream and returning last five lines of it
   * @param {string} the name of the file
   * @return {string} last five lines of the file
   */
  function fiveLiner(file) {
    return new Promise((resolve, reject) => {

      checkThatFileExists(file)
      .catch((err) => {
        reject(err);
      });

      const maxLines = 5;
      const lastLines = [];

      // load the file in a stream and push each line to a separate chunk, then collect 5 last lines
      fs
        .createReadStream(file)
        .pipe(split(/\n/))
        .on('data', (line) => {
          lastLines.push(line);
          if (lastLines.length > maxLines) { lastLines.shift(); }
        })
        .on('error', (err) => { 
          reject(err); 
        })
        .on('end', () => {
          resolve(returnLastLines(lastLines));
        });
    });
  }

  // quit with error if no filename or file does not exist
  function checkThatFileExists(fileName) {
    return new Promise((resolve, reject) => {
      if (!fileName) { reject('No filename was provided!'); }

      fs.access(fileName, (err) => {
        if (err && err.code === 'ENOENT') {
          reject('File does not exist!');
        };
      });
    });
  }

  // quit with error if empty string or return the last 5 lines
  function returnLastLines(lines) {
    return new Promise((resolve, reject) => {
      let lastLines = lines.join('\n');
      if (lastLines == '') { reject('The file is empty, nothing to return!'); }
      resolve(lastLines);
    });
  }

  return {
    fiveLiner: fiveLiner
  };
};