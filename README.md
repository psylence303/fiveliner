fiveliner
=========

# requirements

 * [node.js](https://nodejs.org/) >= 4, [npm](https://www.npmjs.com/) >= 2
 * run `npm i` to install modules

# running

To execute against some file:

`export FILE_NAME=path/to/file.txt && npm start`

To run the automated tests:

`npm test`

To run linter:

`npm run lint`

# libraries used

* [Mocha.js](https://mochajs.org/) as a test runner
* [Chai.js](http://chaijs.com/) for expect-style assertions
* [Split](https://github.com/dominictarr/split) does the magic of splitting the stream like a string
