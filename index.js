import fl from './lib/fiveliner';

const fileName = process.env.FILE_NAME;

function fiveLineThis(pathToFile) {
  fl().fiveLiner(pathToFile)
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
}

fiveLineThis(fileName);
