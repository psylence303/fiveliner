// calling the fiveLiner function
//   ✓ returns last five lines of the file with LF (Unix/OS X) line breaks
//   ✓ returns last five lines of the file with CRLF (Windows) line breaks
//   ✓ returns the proper five lines for the file containing special unicode symbols
//   ✓ returns all four lines for the file which contains only four lines
//   ✓ returns one line of the file which contains only one line
//   ✓ returns an error if no filename was provided
//   ✓ returns an error if the file does not exist
//   ✓ returns an error if the provided file is empty

import fs from 'fs';
import path from 'path';
import chai from 'chai';
import fl from '../lib/fiveliner';

const expect = chai.expect;
const fixturesDir = path.join(__dirname, 'fixtures');

function readStringFrom(fileName) {
  return fs.readFileSync(fileName).toString();
}

function pathForFixture(fileName) {
  return path.join(fixturesDir, fileName);
}

describe ('calling the fiveLiner function', function() {
  it('returns last five lines of the file with LF (Unix/OS X) line breaks', function() {
    let dataFile = pathForFixture('happy_path_lf.txt');
    let expectedResultFile = pathForFixture('happy_path_lf_result.txt');

    return fl().fiveLiner(dataFile)
    .then((result) => {
      expect(result).to.be.eql(readStringFrom(expectedResultFile));
    });
  });

  it('returns last five lines of the file with CRLF (Windows) line breaks', function() {
    let dataFile = pathForFixture('happy_path_crlf.txt');
    let expectedResultFile = pathForFixture('happy_path_crlf_result.txt');

    return fl().fiveLiner(dataFile)
    .then((result) => {
      expect(result).to.be.eql(readStringFrom(expectedResultFile));
    });
  });

  it('returns the proper five lines for the file containing special unicode symbols', function() {
    let dataFile = pathForFixture('unicode.txt');
    let expectedResultFile = pathForFixture('unicode_result.txt');

    return fl().fiveLiner(dataFile)
    .then((result) => {
      expect(result).to.be.eql(readStringFrom(expectedResultFile));
    });
  });

  it('returns all four lines for the file which contains only four lines', function() {
    let dataFile = pathForFixture('four_lines.txt');
    let expectedResultFile = pathForFixture('four_lines_result.txt');

    return fl().fiveLiner(dataFile)
    .then((result) => {
      expect(result).to.be.eql(readStringFrom(expectedResultFile));
    });
  });

  it('returns one line of the file which contains only one line', function() {
    let dataFile = pathForFixture('one_line.txt');
    let expectedResultFile = pathForFixture('one_line_result.txt');

    return fl().fiveLiner(dataFile)
    .then((result) => {
      expect(result).to.be.eql(readStringFrom(expectedResultFile));
    });
  });

  it('returns an error if no filename was provided', function() {
    let dataFile = '';
    let expectedError = 'No filename was provided!';

    return fl().fiveLiner(dataFile)
    .catch((err) => {
      expect(err).to.be.eql(expectedError);
    });
  });

  it('returns an error if the file does not exist', function() {
    let dataFile = pathForFixture('no_such_file.txt');
    let expectedError = 'File does not exist!';

    return fl().fiveLiner(dataFile)
    .catch((err) => {
      expect(err).to.be.eql(expectedError);
    });
  });

  it('returns an error if the provided file is empty', function() {
    let dataFile = pathForFixture('empty_file.txt');
    let expectedError = 'The file is empty, nothing to return!';

    return fl().fiveLiner(dataFile)
    .catch((err) => {
      expect(err).to.be.eql(expectedError);
    });
  });

});